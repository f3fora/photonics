# photonics

University of Trento - Department of Physics  
Laboratory of Advanced Photonics [145283] - 2021  
_Professors_: PAOLO BETTOTTI, DAVIDE BAZZANELLA  
_Authors_: LEONARDO CATTARIN, ALESSANDRO FORADORI, DIEGO PICIOCCHI, FRANCESCO VENTURELLI, GRETA VILLA

## About the Project

This project was assigned during the MSc _Laboratory of Advance Photonics_ course, held in 2021 by Prof. Paolo Bettotti at the University of Trento, and includes:

- the [Report](https://gitlab.com/f3fora/photonics/-/jobs/artifacts/master/file/main.pdf?job=TeX) of a diode laser characterization;
- the [Code](src/photonics/) to manage the OSA and the diode laser;
- a [notebook](main.ipynb) and some [utilities](src/utilities/) for data analysis.

Note that:

- the Python, Bash, Jupyter code is distributed under the license [UNLICENSE](LICENSE);
- the images, the LaTeX code and the related PDF are subject to the license [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/);
- the logo of the University of Trento belongs to the University of Trento.
