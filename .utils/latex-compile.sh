#!/usr/bin/env bash

TEX_DIR=$1
HOME_DIR=$2
if [[ -f "${TEX_DIR}/main.tex" ]]; then
	cd ${TEX_DIR}

	mkdir ${TEX_DIR}/img
	ln ${TEX_DIR}/../img/*.pdf ${TEX_DIR}/img/.
	pdflatex -draftmode main 2>&1 >/dev/null
	biber main 2>&1 >/dev/null
	pdflatex main
    if [[ $? -eq 0 ]]; then
        mv "${TEX_DIR}/main.pdf" ${HOME_DIR}
    else
        echo "pdflatex fails to compile"
        exit 1
    fi
else
    echo "main.tex doesn't exist"
fi

