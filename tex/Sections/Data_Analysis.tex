\section{Data Analysis} \label{sec:data_analysis}

\subsection{Emission Spectrum} \label{ssec:emission_spectrum}

In order to check if the qualitative behaviour is reproduced as expected, each measured intensity profile at different values of temperature and current is plotted, as in \autoref{fig:spectrum_in_dBm}. As the emission occurs on many of the resonant cavity modes, which undergo constructive interference, the spectrum is composed by several peaks in an envelope with a maximum.

\inputfigure{spectrum_in_dBm}%
{Spectrum}%
{Spectrum in a \si{dBm} scale at \SI{45}{\celsius} and \SI{10}{\mA}. The wavelength interval has been restricted to the central peak so that the multimode nature of the spectrum can be observed.}

The maximum of the gain coefficient depends on the excess carrier concentration and, consequently, on the injected current. Thus, the maximum intensity should increase (linearly) with the injected current, provided it is above the threshold value. Additionally, both the quantum efficiency and threshold current should show a temperature dependence, which will be quantitatively discussed later.

\FloatBarrier
\subsection{Power Profiles} \label{ssec:power_profiles}

For each of the chosen temperatures, the output power is analyzed in terms of the injected current. In order to compute the total output power, the data are converted from \si{\dBm} to \si{\mW}, according to the following relation:
\begin{equation}
    P[\si{\mW}] = 10^{\frac{P[\si{\dBm}]}{10}}
\end{equation}
Then, the total power is computed via the integration in $d\lambda$:
\begin{equation} \label{eq:total_power}
    P_{\rm tot} = \frac{\xi}{\delta \lambda _{\rm res}} \int_{\lambda_i}^{\lambda_f} P(\lambda) [\si{\mW}] \dd{\lambda}
\end{equation}
where $\delta \lambda _{\rm res}$ is the OSA resolution\footnote{see also \autoref{ssec:OSA}},
$\xi$ is a correction factor\footnote{The factor $\xi$ provides a correction for the part of the spectrum outside the scanning range. However, the acquired tails are already negligible and thus, the missing points do not contribute significantly to the total power, which can be slightly underestimate, assuming $\xi = 1$.}, which is assumed to be $\xi = 1$ and $\lambda_i =$ \SI{1450}{\nm} and $\lambda_f =$ \SI{1650}{\nm} are the lower and upper limits of the spectrum.

The profiles show a behaviour which is qualitatively similar to the expected one, with a linear increase of the power above the threshold current $I_{\rm th}$. \autoref{fig:powerprofile} provides a graphical view of the results.

\inputfigure{powerprofile}%
{Power Profiles: Examples}%
{The power profiles for some temperatures. The total power is calculated for each spectrum at a given $i$ and $T$ as \autoref{eq:total_power}.}

In order to give a quantitative characterisation, $I_{\rm th}$ and $\alpha$ are calculated as explained in \autoref{fig:slope_T40}.

\inputfigure{slope_T40}%
{Power Profiles: Estimation of Parameters}%
{The power profile for $T = \SI{40}{\celsius}$. The second and third subplots show the computed first and second derivative of the total power. The maximum of the second derivative $\tilde{I}_{\rm th}$, shown as a green vertical line, is used as  an estimation of $I_{\rm th}$. Then the power before $\tilde{I}_{\rm th}$ and the one after are fitted with a straight line, plotted respectively in red and in yellow. Their intersection is assumed to be the best estimation of $I_{\rm th}$ and is reported, with its error, as the area in pink. The angular coefficient of the yellow line is instead the slope $\alpha$.}

The graph in \autoref{fig:slopes_temperature} shows the slope $\alpha$ values above the threshold current for each measurement temperature $T$. The value reaches a maximum around $T=\SIrange{20}{25}{\celsius}$ and decreases at higher temperature. The behaviour is in agreement with the qualitative idea that the laser efficiency decreases at high temperatures, even though the measured excursion is in the order of two error bars.

\inputfigure{slopes_temperature}%
{Slope and External Quantum Efficiency}%
{The plot shows the values of the slope $\alpha$, defined as in \autoref{eq:slope}, and the correspondent external quantum efficiencies $\eta_d$, as in \autoref{eq:efficiency}. The two quantities are approximately proportional, and thus, with the appropriate scale, they are almost overlapped in the graph, because the maximum variation of peak $\lambda$ is in the order of magnitude of $1\%$.}

The temperature dependence of the threshold current $I_{\rm th}$ is quantified by parametrizing \autoref{eq:thresholdtemp} as 
\begin{equation} \label{eq:expfit}
    I_{\rm th} = a \exp(\frac{T}{b})
\end{equation} 
so that the $a$ parameter corresponds to a generic $I_0$ current, while $b$ is referred to the characteristic temperature $T_0$.
\autoref{fig:characteristic_temperature} represents both the experimental data and the fitted model. The results of the fit are shown in \autoref{tab:exp}.

\inputfigure{characteristic_temperature}%
{Characteristic Temperature}%
{The plot shows both the data and the fit for the threshold current $I_{\rm th}$ versus the temperature $T$.}

\begin{table}[htbp]
    \centering
    \begin{tabular}{c|c}
        $I_0$ [\si{\mA}] & $T_0$ [\si{\celsius}]\\   
        \hline
        \num{7.7(1)} & \num{68(2)}
    \end{tabular}
    \caption{The table reports the fit results for \autoref{eq:expfit}, where $a \equiv I_0$ and $b \equiv T_0$.}
    \label{tab:exp}
\end{table}

\FloatBarrier
\subsection{Peak Wavelength} \label{ssec:peak_wavelength}

As the temperature of the laser increases, a shift in the optical gain shape takes place, ultimately resulting in a change in the relative amplitude of the resonance peaks in the final spectral profile of the laser. The results obtained measuring the peak wavelength are shown in \autoref{fig:peak_temperature23}. As expected, a stair-like behaviour is present, alternating weak linear increases and sudden jumps of $\Delta\lambda_{hop} = $\SI{1.2(1)}{\nm}. This last effect is the so called \emph{mode-hopping}. 

\inputfigure{spectrum_smooth}%
{Peak Wavelength: Calculation}%
{In order to extract the maximum peak and the maximum wavelength, a smoothing procedure in employed. Averaging over a window of a given length, we produce the red curve and then locate its maximum, which corresponds the peak we are looking for. }

\inputfigure{peak_temperature23}%
{Mode-Hopping}%
{Peak wavelength $\lambda_p$ as a function of T at \SI{23}{\mA}. The sudden jumps in wavelength can be considered as the result of mode-hopping.}

\FloatBarrier
\subsection{Peak separation} \label{ssec:peak_separation}

The last parameter which is extracted for the characterization is the \emph{peak separation} $\Delta \lambda_p$, computed in terms of wavelengths for different values of temperature. This parameter quantifies the distance between the peaks of the relative intensity spectrum produced by the resonator of the laser. It can be related to the resonating cavity parameters with the following equation:
\begin{equation}
    \Delta \lambda_p = \frac{\lambda_m \lambda_{m+1}}{2nd} \approx \frac{\lambda^2}{2nd}
\end{equation}
where $\lambda_m$, $\lambda_{m+1}$ are the wavelengths corresponding to the peaks of order $m$ and $m+1$ (with $\lambda \approx \lambda_m \approx \lambda_{m+1}$). $d$ is the length of the resonator cavity and $n$ is the refractive index inside it, which are characteristic parameters of the laser in use and are both unknown to us.  $\Delta \lambda_p$ is estimated as the average between the spectral distance of the 8 peaks around the central one. An example of the computed peaks is shown in the inset of \autoref{fig:find_peaks}. 
Then, by evaluating $\Delta \lambda_p$ on all measurements, the distribution shown in \autoref{fig:dlambda_dist} is produced. It is clear that the estimated values for $i<I_{\rm th}$ are essentially random, since the lasing condition is only verified for $i>I_{\rm th}$. This is highlighted in both \autoref{fig:dlambda_dist_I} and \autoref{fig:dlambda_dist_T} and is confirmed by the median of the distribution computed both keeping all data and keeping only those for $i>I_{\rm th}$, as shown in \autoref{tab:dlambda}:

\inputfigure{find_peaks}%
{Peak Identification}%
{Blue points and lines correspond to the measured power spectrum while the red ones to the peaks considered to evaluate the peak separation $\Delta \lambda_p$.}

\begin{table}[htbp]
    \centering
    \begin{tabular}{c|c|c}
            & mean[\si{\nm}] & median[\si{\nm}]\\    
    \hline
    $\forall i$ & \num{1.181} & \num{1.191} \\
    $i>I_{\rm th}$ & \num{1.192} & \num{1.191}\\
    \end{tabular}
    \caption{Mean and median of the distribution of $\Delta \lambda_p$ evaluated on all data or only for $i>I_{\rm th}$.}
    \label{tab:dlambda}
\end{table}

\inputfigure{dlambda_dist}%
{Peak Separation Distribution}%
{Distribution of all the measured $\Delta \lambda_p$. The histogram and upper boxplot result from all data. 
The lower boxplot shows the distribution only for $i>I_{\rm th}$, which is considerably narrower. In the boxplots, the red line identifies the median, the yellow triangle is the mean and the range delimited by the black bars represents the one between the first and the third quantile.}

\inputfigure{dlambda_dist_T}%
{Peak Distribution as a Function of Temperature}%
{$\Delta \lambda_p$ shows a linear increase in the temperature, even though the values collected with $i<I_{\rm th}$ have a random behavior.}

\inputfigure{dlambda_dist_I}%
{Peak Distribution as a Function of Injected Current}%
{The plot shows the distribution of $\Delta\lambda_p$ as a function of current. The distribution starts to stabilize from $i = \SI{9.3(1)}{\mA}$, the value of $I_{\rm th}$ for $T=\SI{15}{\celsius}$.}

\FloatBarrier
\subsection{Systematic errors and problems}  \label{ssec:systematic_errors}

In the course of the experience, we encountered some systematic problems due to the devices employed. Hereafter follows a discussion of the two most relevant issues.

\FloatBarrier
\subsubsection{OSA losses}  \label{ssec:osa_losses}

During data analysis, a discrepancy between the expected value of the power and the obtained one is observed, meaning that some power losses may have occurred. A possible source of such losses could lie in the OSA device itself, where some internal losses decrease the total power measured at the output.

\FloatBarrier
\subsubsection{Manually setting the resolution}\label{ssec:osa_res}

As previously anticipated, a characteristic of the OSA is that it allows the user to manually set the desired parameters for resolution and number of sampling points. This means that having chosen a range of wavelengths and a certain hardware resolution $\delta \lambda_{res}$, it is possible to select different values of sampling spacing $\delta \lambda_{samp}$. This aspect is not be overlooked since it has strong consequences in the accurate reconstruction of the spectrum and, consequently, on the computation of the characterisation parameters.

If the desired resolution $\delta \lambda_{res}$ is \emph{too big} with respect to the number of sampled points, adjacent samples will contain power contributions coming from overlapping portions of the spectrum. However, since the laser spectrum is composed by peaks, the OSA will assign the same value to every sampled point, within resolution $\delta \lambda_{res}$ from a peak. This results in the spectrum being composed of a series of small ``plateaus'' (e.g., ten adjacent points, all with the same value).

When this occurs, a problem arises during the integration of the spectrum: these points are all summed over, risking to over estimate or evaluate wrongly the total power. One way of possibly recovering an acceptable estimate of the desired quantities involves the normalization of the integral, which manages to reduce the error.

During the experience, a first set of measurements was obtained with a resolution set too big, resulting in the problematic effect just described. 
A second set of data has been collected with a more reasonable choice of parameters: in particular, $N_{pt}$ was chosen such that $\delta \lambda_{samp}$ was as close as possible to the value of $\lambda_{res}$.
Such data have been used for the data analysis discussed in this section. 

%%% Images of the two resolutions
\inputfigure{resolution_comparison}%
{Resolution Comparison}%
{Spectrum at $T=\SI{49}{\celsius}$ and $I=\SI{23.3}{\mA}$ in the case of oversampling (blue points) and in the case of $\delta \lambda_{\rm samp} \approx \delta\lambda_{\rm res}$ (red points). % wrong
The legend indicates the value of $\delta \lambda_{\rm res}$.}

\inputfigure{oversampling_evidence}%
{Resolution Comparison in the Central Peak}%
{Zoom in on the central peak. Here it is easier to see the oversampling in the first set of measurement. The continuous line is a simple interpolation between the points, used to improve visualization.}

\autoref{fig:resolution_comparison} shows a graphic representation of the phenomenon described above: the oversampling is clear looking at the shape of the peaks in the insets.


