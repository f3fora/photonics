import numpy as np
from scipy.constants import speed_of_light, Planck, elementary_charge


def EQE(slope_efficiency, dslope_efficiency, wl):
    """
    Evaluate the total external quantum efficiency at the peak of wavelength wl.

    Params:
    slope_efficiency :  np.array
                        slope efficiency of the laser
    dslope_efficiency : np.array
                        slope efficiency error
    wl : np.array
         laser wavelengths in nm at different temperatures

    Returns:
    eqe : np.array
          external quantum efficiency
    deqe : np.array
           propagated error
    """
    wl = np.array(wl) * 1e-9  # convert to meters
    eqe = 2 * slope_efficiency * elementary_charge * wl / (Planck * speed_of_light)
    deqe = 2 * dslope_efficiency * elementary_charge * wl / (Planck * speed_of_light)
    return eqe, deqe
