from matplotlib import pyplot as plt
from matplotlib import font_manager as font_manager, get_data_path


def set_size(width=400, fraction=1, subplots=(1, 1), portrait=False):
    """
    https://jwalton.info/Embed-Publication-Matplotlib-Latex/
    """
    width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5 ** 0.5 - 1) / 2
    if portrait:
        golden_ratio = (5 ** 0.5 + 1) / 2

    ratio = (subplots[0] / subplots[1]) * golden_ratio

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * ratio

    return (fig_width_in, fig_height_in)


def save_figure(f, fname, path, extension="pdf"):
    """ """
    # save a matplotlib figure
    f.savefig(
        (path / fname).with_suffix(f".{extension}"),
        dpi=f.dpi,
        format=extension,
        transparent=True,
        bbox_inches="tight",
    )


def set_style():
    plt.style.use("fivethirtyeight")

    SMALL_SIZE = 9
    MEDIUM_SIZE = 11
    BIG_SIZE = 13
    cmfont = font_manager.FontProperties(fname=f"{get_data_path()}/fonts/ttf/cmr10.ttf")

    return plt.rcParams.update(
        {
            "text.usetex": True,
            "font.family": "serif",
            "font.serif": cmfont.get_name(),
            "mathtext.fontset": "cm",
            "axes.unicode_minus": False,
            "figure.figsize": set_size(),
            "font.size": SMALL_SIZE,
            "axes.titlesize": SMALL_SIZE,
            "axes.labelsize": MEDIUM_SIZE,
            "xtick.labelsize": SMALL_SIZE,
            "ytick.labelsize": SMALL_SIZE,
            "legend.fontsize": SMALL_SIZE,
            "figure.titlesize": BIG_SIZE,
            "lines.linewidth": 2,
            "lines.markersize": 3,
            "axes.xmargin": 0.05,
            "axes.ymargin": 0.11,
        }
    )
