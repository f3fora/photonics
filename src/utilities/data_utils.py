""" Here are defined two classes to load and manipulate effciently the measurments. """

from pathlib import Path

import numpy as np
import pandas as pd
from scipy.signal import find_peaks

# These just identifies the ROOT directory
ROOT = Path(__file__)  # directory of the current file
ROOT = (
    ROOT.parent.parent.parent
)  # some levels of parent direcotory to get to /photonics/


def _mean_std_after(x, t_start, x_approx):
    """
    This function evaluates the mean and std of an array or iterable. The x_std is choosen
    as the largest value between the difference of the evaluated mean and the expected value
    x_approx.

    Params:
    x : np.array of size [n, 2]
        containing the measurments(column 1) and the time of the measurment(column 0)

    t_start : int, in seconds from the epoch
              time of the first measure

    x_approx : float
               nominal value of the occuring measurment

    Returns:
    x_mean : float, mean value

    x_std : float, standard deviation

    """
    x_true = x[x[:, 0] > t_start]  # picks all the measurments but the first one
    x_mean = x_true[:, 1].mean()
    x_std = x_true[:, 1].std()
    # choose the largest
    x_std = x_std if np.abs(x_mean - x_approx) < x_std else np.abs(x_mean - x_approx)
    return x_mean, x_std


def dBm2mW(P_dBm):
    return np.power(10, (P_dBm / 10))


def mW2dBm(P_mW):
    return 10 * np.log10(P_mW)


class SingleMeasure:
    """
    Class to extrapolate the data for a single measurment i.e. a single temperature-current pair
    that is a singla data folder.

    The measures returned are:
    - power and total_power in mW
    - power_in_dBm in dBm
    - wavelength in nm
    - current in mA
    - temperature in *C
    """

    def __init__(self, path):
        """
        This is just the initialization on the data folder direcotry.
        The measurment metadata are taken from the OSA queries.
        """
        # data directory
        self._path = path
        index = np.loadtxt(self._path / "index.csv", delimiter=",")
        # these values are autmatically found from the values in the index.csv file
        self.start_time = index[0]  # start time
        self.min_wl = index[1]  # lower bound of OSA full scale
        self.max_wl = index[2]  # upper bound of OSA full scale
        self.n_points = index[3]  # number of points measured by the OSA
        self.n_avg = index[4]  # number of averages
        self.resolution = 1.0 if index.size < 6 else index[5]  # resolution in nm

        self.n_points = self.n_points.astype(int)  # np.loadtxt default type is float
        self.n_avg = self.n_avg.astype(int)

    # This is a flag used in classes to idetify as function as a value to be retrieved
    # It also implies a functio to set the value
    @property
    def current(self):
        """
        Loads current form the right .csv file and converts it in mA
        Returns a np.array
        """
        return np.loadtxt(self._path / "current.csv", delimiter=",") * 1e3

    @property
    def temperature(self):
        """
        Loads temperature form the right .csv file and returns it in *C
        Returns a np.array
        """
        return np.loadtxt(self._path / "temperature.csv", delimiter=",")

    @property
    def wavelength(self):
        """
        Returns a np.array covering the OSA range.
        """
        return np.linspace(self.min_wl, self.max_wl, num=self.n_points, endpoint=True)

    @property
    def power_in_dBm(self):
        """
        Loads the OSA measured spectrum form the right .csv file and returns it in dBm.
        Returns a np.array.
        """
        return np.loadtxt(self._path / "spectrum.txt", delimiter=",") * 1e-2

    @property
    def power(self):
        """
        Converts the spectrum from dBm to mW.
        Returns a np.array
        """
        return np.power(10, (self.power_in_dBm / 10))

    @property
    def mean_current(self):
        """
        Self explanatory.
        Returns two float values current_mean, current_std
        """
        return _mean_std_after(
            self.current,
            self.start_time,
            self.approx_current,
        )

    @property
    def approx_current(self):
        """
        Extracts the nominal value of the current from the folder name.
        The variable self._path gives the directory used for initialazation in the path
        variable and the attribute name gives a string with the last subdirectory
        """
        # slice the current value from the string adn convert to float
        return float(self._path.name[4:])

    @property
    def mean_temperature(self):
        """
        Self explanatory
        returns temperature_mean, temperature_std as floats
        """
        return _mean_std_after(
            self.temperature,
            self.start_time,
            self.approx_temperature,
        )

    @property
    def approx_temperature(self):
        """
        Extracts the nominal value of the temperature from the folder name.
        The variable self._path gives the directory used for initialazation in the path
        variable and the attribute name gives a string with the last subdirectory
        """
        # Slicing only the value and converting to float
        return float(self._path.name[1:3])

    @property
    def total_power(self):
        """
        Integration with the trapezoidal rule of the power in mW over the wavelngth
        normalized by the supposed resolution FullScale/n_points.

        Returns a float
        """
        return np.trapz(self.power, x=self.wavelength) / self.resolution

    @property
    def total_power_in_dBm(self):
        """
        Integration with the trapezoidal rule of the power in dBm over the wavelngth
        normalized by the supposed resolution FullScale/n_points.

        Returns a float
        """
        return mW2dBm(self.total_power)

    @property
    def wavelength_peak(self, N=100):
        """
        Extrapolate the position of the peak from the raw dBm spectrum.

        Returns a flaot corresponding to the wavelength of the peak.
        """

        I = self.power_in_dBm.T  # transpose it so that pd.Series likes it

        # rolling : selects a window on which to do the statistics after it
        # basically here I is meaned 100 samples at a time, NaN are discarded and the new
        # (size(I)/100 - discarded) values are kept. This is a smoothing
        cleanI = (
            pd.Series(I).rolling(window=N, center=True).mean().fillna(I.min()).values
        )
        # take the index corresponding to the maximum
        i_max = cleanI.argmax()
        return self.wavelength[i_max]

    def find_peaks(self, around_peak=None, *args, **kwargs):
        """
        Find the position of the peaks around the central peak usign scipy.find_peaks.

        Params:
        around_peak : float,
                      half length of the interval to consider around the main peak
        *args, **kwargs : additional variables acepted by scipy.find_peaks

        Returns  the peaks in wavelength, the corresponding powewrs and a dlambda mean value.

        """
        # return nothing if no central peak is provided
        if around_peak is None:
            mmap = None
        else:
            # define a mask mmap around the central peak
            wl_p = self.wavelength_peak
            mmap = (wl_p - around_peak / 2 < self.wavelength) & (
                self.wavelength < wl_p + around_peak / 2
            )
        # use scipy.find_peaks on the power spectrum masked with mmap
        peaks, _ = find_peaks(self.power[mmap].squeeze(), *args, **kwargs)

        return (
            self.wavelength[mmap][peaks],  # wavelength of the peaks
            self.power[mmap][peaks],  # powers
            pd.Series(self.wavelength[mmap][peaks])
            .diff()
            .dropna()
            .mean(),  # mean distance of two peaks
        )


class SetOfMeasures:
    """
    Combines more objects of the class SingleMeasurment so that all data can be adressed easily
    with the nominal temperature and current values.
    """

    def __init__(self, path):
        """
        Initializes with the data path.
        """
        self._path = path
        # loads the single measurments as a list of objects
        # self._path.glob("*") returns a list with all the paths under the self._path
        self._measures = [
            SingleMeasure(path) for path in self._path.glob("*") if path.is_dir()
        ]

        # These are just initialization of private variables that each ahve a defining function
        self._currents = None
        self._approx_currents = None

        self._temperatures = None
        self._approx_temperatures = None

        self._powers = None
        self._powers_in_dBm = None

        self._wl_peaks = None
        self.wl_peaks_FWHM = None

        self._spectral_range = None

        self._order = None  # initialiazes index_array
        # after all the private variables are evaluated it produces a proper index array
        # np.lexsort(a,b) sorts the elements of b and a as ascending pairs
        self._order = np.lexsort((self.approx_temperatures, self.approx_currents))

    @property
    def currents(self):
        """
        Collects the mean value of the measured mean currents and std during a single measurment.
        Returns a (N,) np.array containing the currents ordered by the nominal temperature and
        current.
        """
        if self._currents is None:
            # redifined only at initialization
            self._currents = np.array(
                [measure.mean_current for measure in self._measures]
            )

        return self._currents[self._order].squeeze()

    @property
    def approx_currents(self):
        """
        Collects the nominal value of the current for each single measurment.
        Returns a (N,) np.array containing the nominal currents, ordered.
        """
        if self._approx_currents is None:
            # redifined only at initialization
            self._approx_currents = np.array(
                [measure.approx_current for measure in self._measures]
            )

        return self._approx_currents[self._order].squeeze()

    @property
    def temperatures(self):
        """
        Collects the mean value of the measured mean temperature adn std during a single measurment.
        Returns a (N,) np.array containing the temperaatures ordered by the
        nominal temperature and current.
        """
        if self._temperatures is None:
            # redefined only at initialization
            self._temperatures = np.array(
                [measure.mean_temperature for measure in self._measures]
            )

        return self._temperatures[self._order].squeeze()

    @property
    def approx_temperatures(self):
        """
        Collects the nominal value of the measured temperature during a single measurment.
        Returns a (N,) np.array containing the temperaatures ordered by the
        nominal temperature and current.
        """
        if self._approx_temperatures is None:
            # redefined only at initialization
            self._approx_temperatures = np.array(
                [measure.approx_temperature for measure in self._measures]
            )

        return self._approx_temperatures[self._order].squeeze()

    @property
    def powers(self):
        """
        Collects the total powers in mW of each single measurment.
        Returns a (N,) np.array containing the total powers ordered by the
        nominal temperature and current.
        """
        if self._powers is None:
            # redifined only at initialization
            self._powers = np.array([measure.total_power for measure in self._measures])

        return self._powers[self._order].squeeze()

    @property
    def powers_in_dBm(self):
        """
        Collects the total powers in dBm of each single measurment.
        Returns a (N,) np.array containing the total powers ordered by the
        nominal temperature and current.
        """
        if self._powers_in_dBm is None:
            # redifined only at initialization
            self._powers_in_dBm = np.array(
                [measure.total_power_in_dBm for measure in self._measures]
            )

        return self._powers_in_dBm[self._order].squeeze()

    @property
    def wl_peaks(self):
        """
        Collects the peak wavelength of each single measurment.
        Returns a (N,) np.array containing the total powers ordered by the
        nominal temperature and current.
        """
        if self._wl_peaks is None:
            # redifined only at initialization
            self._wl_peaks = np.array(
                [measure.wavelength_peak for measure in self._measures]
            )

        return self._wl_peaks[self._order].squeeze()

    @property
    def spectral_range(self):
        """
        Collects the mean dlambda from all the measurments.
        Returns a (N,) np.array containing them order by the nominal temperature and current.
        """
        if self._spectral_range is None:
            # redifined only at initialization
            self._spectral_range = np.array(
                [
                    measure.find_peaks(around_peak=40, distance=8)[2]
                    for measure in self._measures
                ],
            )

        return self._spectral_range[self._order].squeeze()

    def at(self, temperature=None, current=None):
        """
        Finds the indeces corresponding to a nominal value of temperature or current
        or the index of a temperature-current pair.
        Returns an index_array. Squeeze reduces the dimensionality.
        """
        if temperature is None and current is None:
            return None
        elif temperature is None:
            # finds the indeces only for the currents values
            index = np.argwhere(current == self.approx_currents).squeeze()
        elif current is None:
            # finds the indeces only for the temperature values
            index = np.argwhere(temperature == self.approx_temperatures).squeeze()
        else:
            # both >> all is a built_in_function
            both = np.vstack([self.approx_currents, self.approx_temperatures])
            # finds indeces for the current-temperature pair
            index = np.argwhere(
                (current == both[0]) & (temperature == both[1])
            ).squeeze()

        if index.size == 0:  # if no match is found raises an error
            raise KeyError("Missing Index")

        return index

    def spectrum(self, index=0, at_temperature=None, at_current=None):
        """
        Retrives the wavelength array and power array(in mW) corresponding to
        a given single measurment. The measurment can be decided both by index or
        by temperature-current pair.
        Returns a n size np.array of size 2 lists with
        wavelength(position 0) and power(position 1)

        Example:
        In: sm.spectrum(at_temperature=25, at_current=13.5)
        Out: array([[1.45000000e+03, 1.72583789e-08],
                    [1.45004000e+03, 4.12097519e-08],
                    [1.45008000e+03, 8.93305484e-09],
                    ...,
                    [1.64992000e+03, 2.90402265e-08],
                    [1.64996000e+03, 1.00000000e-12],
                    [1.65000000e+03, 1.00000000e-12]])
        """
        if at_temperature is not None and at_current is not None:
            # checks if it has to compute the index using the self.at function
            index = self.at(at_temperature, at_current)
            if index == None:
                raise KeyError
        # spectrum is a N array of size 2 lists
        spectrum = np.vstack(
            [self._measures[index].wavelength, self._measures[index].power]
        ).T

        return spectrum

    def spectrum_in_dBm(self, index=0, at_temperature=None, at_current=None):
        """
        Retrives the wavelength array and power array(in dBm) corresponding to
        a given single measurment. The measurment can be decided both by index or
        by temperature-current pair.
        Returns a n size np.array of size 2 lists with
        wavelength(position 0) and power(position 1)
        """
        if at_temperature is not None and at_current is not None:
            # checks if it has to compute the index using the self.at function
            index = self.at(at_temperature, at_current)
            if index == None:
                raise KeyError
        # spectrum is a N array of size 2 lists
        spectrum = np.vstack(
            [self._measures[index].wavelength, self._measures[index].power_in_dBm]
        ).T

        return spectrum


# test if it works launching as a script
if __name__ == "__main__":
    data = ROOT / "data" / "2110"
    sm = SetOfMeasures(data)

    print(f"Spectrum {sm.spectrum().shape}")
    print(f"Currents {sm.currents}")
    print(f"Temperatures {sm.temperatures.shape}")
    print(f"Powers {sm.powers.shape}")
