import numpy as np
from scipy.optimize import curve_fit


def p0_fn(x, a):
    """
    Oder 0 polynomial.

    Params:
    x : np.array or iterable
    a : float, constant parameter

    Returns:
    0 order polynomial as np.array of size size(x)
    """
    return np.ones_like(x) * a


def p1_fn(x, a, b):
    """
    Order 1 polynomial.

    Params:
    x : np.array or iterable
        data
    a : float
        0 order parameter, offset
    b : float
        linear order parameter, slope

    Returns:
    np.array size(x)
    """
    return a + b * x


def exp_fn(x, a, b):
    return a * np.exp(x / b)


def fit(*args, **kwargs):
    """
        Generic curve fit from scipy.optimize that returns the parameters and the errors.
        It fits the function of x over y

        Params:
        fn : function
        x : np.array size(N)
            x-data
        y : np.array
            y-data
    temperatures
        Returns:
        params.sqeeze() : np.array size(N)
                          parametes of the fitted function
        np.sqrt(np.diag(cov)).squeeze() : np.array size(N)
                                          parameters std evaluated as sqrt of the diagonal elements
                                          of the covarince matrix cov

    """
    params, cov = curve_fit(*args, **kwargs)
    return params.squeeze(), np.sqrt(np.diag(cov)).squeeze()


def first_second_gradient(x, y):
    """
    First order and second order grandients of y in respect of x

    Params:
    x : np.array
        x-data
    y : np.array
        y-data

    Returns:
    dy : np.array
         first order gradient
    d2y : np.array
          second order gradient
    """
    dy = np.gradient(y, x)
    d2y = np.gradient(dy, x)
    return dy, d2y


def approx_x0(d2y, x, x_min, x_max):
    """
    Finds the change of slope in the data y checking the second order derivative in a
    defined interval.

    Params:
    d2y : np.array
          second derivative of data
    x : np.array
        x data
    x_min : float
            lower limit of the considered interval
    x_max : float
            upper limit of the considered interval

    Returns:
    x[x_range][index] : float
                        x at which d2y is peaked, i.e. change in slope of linear fit
    """
    # define the interval as a boolean mask
    x_range = (x_min < x) & (x < x_max)
    # only inside the values of d2y identified as True by the mask find the index of the maximum
    index = d2y[x_range].argmax()
    return x[x_range][index]


def my_fit(approx_x0, x, y):
    """
    Custom fit. The date are divided between the constant part and the linear part.

    Params:
    approx_x0 : float
                result of approx_x0, position of the change of slope
    x : np.array
        x-data
    y : np.array
        y-data

    Returns:
    np.array([x0, p1, *p2]) : np.array
                              parameters resulting from constant(p1) and linear(p2) fits and
                              x0 interpolated value of the change of slope
    np.array([dx0, dp1, *dp2]) : np.array
                                 errors on the fit results
    """
    # logic mask to separate the constant behavior
    mask1 = x < approx_x0
    # constant fit
    p1, dp1 = fit(p0_fn, x[mask1], y[mask1])
    # logic mask for linear behavior
    mask2 = x > approx_x0
    # linear fit
    p2, dp2 = fit(p1_fn, x[mask2], y[mask2])
    # interpolation between domains to find start of slope( x0*p2[1]+p2[0]=p1)
    x0 = (p1 - p2[0]) / p2[1]
    # error propagation
    dx0 = x0 * (
        dp2[1] / p2[1] + np.sqrt(np.square(dp1) + np.square(dp2[0])) / (p1 + p2[0])
    )

    return np.array([x0, p1, *p2]), np.array([dx0, dp1, *dp2])
