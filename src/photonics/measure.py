import asyncio
import logging
from pathlib import Path
import time

import numpy as np
import pyvisa

from OSA import OSA, OSAParameters, b2s
from laser import LaserfromInstrument

ROOT = Path(__file__)
ROOT = ROOT.parent.parent.parent


class ConfigError(Exception):
    pass


async def single_measure(
    osa,
    laser,
    dirname,
    subdirname,
    I,
    T,
    Itol,
    Ttol,
    min_wl,
    max_wl,
    n_points,
    n_avg,
    resolution,
):
    osa.clear()
    osa.clear_measure()
    laser.clear()
    logging.info(f"[GENERAL] START SINGLE MEASURE {subdirname}")
    here = dirname / subdirname
    here.mkdir(exist_ok=True)
    laser.set_output_files(to=here / "temperature.csv", co=here / "current.csv")

    osa.set_output_files(index=here / "index.csv", spectrum=here / "spectrum.txt")

    # Set Laser Parameters
    logging.info("[LASER] SET PARAMETERS")
    temp_task = asyncio.create_task(
        laser.set_temperature(T=T, atol=Ttol, n_stability=20)
    )
    curr_task = asyncio.create_task(laser.set_current(I=I, atol=Itol, n_stability=5))

    logging.info("[OSA] SET PARAMETERS")
    task_osa_config = asyncio.create_task(
        osa.set_params(resolution, min_wl, max_wl, n_points, n_avg)
    )

    await temp_task
    await curr_task
    await task_osa_config

    logging.info("[ALL] END SET")

    logging.info("[ALL] START DATA ACQUISITION")
    flag = asyncio.Event()
    flag.set()
    temp_task = asyncio.create_task(
        laser.get_continue_temperature(T=T, atol=Ttol, flag=flag)
    )

    curr_task = asyncio.create_task(
        laser.get_continue_current(I=I, atol=Itol, flag=flag),
    )
    task = asyncio.create_task(osa.get_spectrum())
    await task
    flag.clear()
    await temp_task
    await curr_task
    logging.info("[ALL] END ACQUISITION")


async def full_measure():

    rm = pyvisa.ResourceManager()
    instrument = rm.open_resource("USB0::4883::32847::M00307175::0::INSTR")
    OSAParameters["port"] = "/dev/ttyUSB0"

    logging.info("[OSA] INITIALIZATION")
    osa = OSA(**OSAParameters)
    logging.info(f"[OSA] IDN:{b2s(osa.query('*IDN?'))}")
    osa.buz_off()

    logging.info("[LASER] INITIALIZATION")
    laser = LaserfromInstrument(instrument)
    logging.info(f"[LASER] IDN:{laser.query('*IDN?')}")
    laser.turn_laser(True)
    laser.turn_temperature(True)

    data = ROOT / "data"
    data.mkdir(parents=True, exist_ok=True)

    Iarray = np.hstack(
        [np.arange(0, 9, 1), np.arange(9, 15, 0.5), np.arange(15, 28, 1)]
    )
    Tarray = np.arange(15, 49, 5)
    Iarray = [23.3]
    Tarray = np.arange(15, 49, 1)
    for T in Tarray:
        for I in Iarray:
            # try for maximum ten times the same measure
            # when one succeed, continue
            # if all attempts fail, break all loops
            for i in range(10):

                logging.info(f"START: {time.ctime()}")
                task = asyncio.create_task(
                    single_measure(
                        osa=osa,
                        laser=laser,
                        dirname=data,
                        subdirname=f"T{T}I{I}",
                        I=I * 1e-3,
                        T=T,
                        Itol=1e-3,
                        Ttol=5e-2,
                        min_wl=1450.0,
                        max_wl=1650.0,
                        n_points=2001,
                        n_avg=4,
                        resolution=0.1,
                    )
                )
                try:
                    await task
                    logging.info(f"END: {time.ctime()}")
                except Exception as e:
                    logging.error(f"fail for the {i+1} times for {e}")
                    continue

                break

            else:
                logging.error("fail too many time")
                break
        else:
            continue

        break

    logging.info("[LASER] SHUTDOWN")
    laser.turn_laser(False)
    laser.turn_temperature(False)


if __name__ == "__main__":
    logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)
    logging.info(f"{ROOT}")
    asyncio.run(full_measure())
