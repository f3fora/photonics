import asyncio
import logging
import time

import numpy as np
import pyvisa


Instrument = pyvisa.resources.USBInstrument


class TemperatureError(Exception):
    pass


class CurrentError(Exception):
    pass


class LaserError(Exception):
    pass


class Laser(Instrument):
    def __init__(self, *args, **kwargs):
        self.temperature_output = None
        self.current_output = None
        super().__init__(*args, **kwargs)

    def set_output_files(self, to, co):
        self.temperature_output = to
        self.current_output = co

    def errors(self, cmd="SYSTem:ERRor:NEXT?"):
        err = self.query(cmd)
        if '+0,"No error"' in err:
            return True

        raise LaserError(err)

    def clear(self, cmd="*CLS"):
        return self.write(cmd)

    def turn_temperature(self, on, cmd="OUTPut2:STATe"):
        status = "ON" if on else "OFF"
        return self.write(f"{cmd} {status}")

    def turn_laser(self, on, cmd="OUTPut1:STATe"):
        status = "ON" if on else "OFF"
        return self.write(f"{cmd} {status}")

    def my_query(self, read, file=None):
        x_mes = self.query(read)
        if file is not None:
            with open(file, "a") as f:
                f.write(f"{int(time.time_ns() * 1e-6)},{x_mes}")  # time in ms

        return float(x_mes)

    async def _wait(
        self,
        x_want: float,
        n_stability: int,
        atol: float,
        sleep_time: float,
        write: str,
        read: str,
        file,
    ):
        if file is not None:
            with open(file, "w+") as _:
                pass

        self.write(write + " " + str(x_want))
        counter = 0
        while True:
            await asyncio.sleep(sleep_time)
            x_mes = self.my_query(read, file)
            if np.isclose(x_want, x_mes, atol=atol):
                if counter >= n_stability:
                    break

                counter += 1

            else:
                counter = 0

        return self.errors()

    async def set_temperature(self, T, atol, n_stability):
        return await self._wait(
            x_want=T,
            n_stability=n_stability,
            atol=atol,
            sleep_time=0.01,
            write="SOURce2:TEMPerature:SPOint",
            read="SENSe2:TEMPerature:DATA?",
            file=self.temperature_output,
        )

    async def set_current(self, I, atol, n_stability):
        return await self._wait(
            x_want=I,
            n_stability=n_stability,
            atol=atol,
            sleep_time=0.01,
            write="SOURce1:CURRent:LEVel:AMPLitude",
            read="SENSe3:CURRent:DATA?",
            file=self.current_output,
        )

    async def _continue(
        self,
        x_want: float,
        atol: float,
        sleep_time: float,
        cmd: str,
        file,
        flag: asyncio.Event,
        error,
    ):
        while flag.is_set():
            await asyncio.sleep(sleep_time)
            x_mes = self.my_query(cmd, file)
            if not np.isclose(x_want, x_mes, atol=atol):
                raise error(f"x_mes [{x_mes}] is not close to x_want [{x_want}]")

        return x_want

    async def get_continue_temperature(self, T, atol, flag):
        return await self._continue(
            x_want=T,
            atol=atol,
            sleep_time=0.1,
            cmd="SENSe2:TEMPerature:DATA?",
            file=self.temperature_output,
            error=TemperatureError,
            flag=flag,
        )

    async def get_continue_current(self, I, atol, flag):
        return await self._continue(
            x_want=I,
            atol=atol,
            sleep_time=0.1,
            cmd="SENSe3:CURRent:DATA?",
            file=self.current_output,
            error=CurrentError,
            flag=flag,
        )


def LaserfromInstrument(instrument: Instrument = None) -> Laser:
    if instrument is None:
        rm = pyvisa.ResourceManager()
        instrument = rm.open_resource(rm.list_resources()[0])

    instrument.__class__ = Laser
    return instrument


if __name__ == "__main__":
    rm = pyvisa.ResourceManager()
    laser = LaserfromInstrument(rm.open_resource(rm.list_resources()[0]))

    async def test_set():
        temp_task = asyncio.create_task(
            laser.set_temperature(T=25.0, atol=1e-3, n_stability=5)
        )
        curr_task = asyncio.create_task(
            laser.set_current(I=27e-3, atol=1e-3, n_stability=5)
        )
        await asyncio.wait({temp_task, curr_task}, return_when=asyncio.FIRST_EXCEPTION)
        return temp_task.result(), curr_task.result()

    async def test_continue():
        flag = asyncio.Event()
        flag.set()
        temp_task = asyncio.create_task(
            laser.get_continue_temperature(T=25.0, atol=1e-1, flag=flag)
        )

        curr_task = asyncio.create_task(
            laser.get_continue_current(I=27e-3, atol=1e-3, flag=flag),
        )

        await asyncio.sleep(5.0)
        flag.clear()

        try:
            return await temp_task, await curr_task
        except Exception as e:
            return e

    async def test_fail():
        flag = asyncio.Event()
        flag.set()
        temp_task = asyncio.create_task(
            laser.get_continue_temperature(T=28.0, atol=1e-1, flag=flag)
        )

        curr_task = asyncio.create_task(
            laser.get_continue_current(I=27e-3, atol=1e-3, flag=flag),
        )

        await asyncio.sleep(5.0)
        flag.clear()

        try:
            return await temp_task, await curr_task
        except Exception as e:
            return e

    logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)
    logging.info(rm.list_resources())

    logging.info(laser.query("*IDN?"))
    logging.info(f"ERROR: {laser.errors()}")
    laser.clear()
    logging.info(f"ERROR: {laser.errors()}")
    laser.turn_laser(True)
    laser.turn_temperature(True)

    logging.info(f"SET: {asyncio.run(test_set())}")
    logging.info(f"CONTINUE: {asyncio.run(test_continue())}")
    logging.info(f"FAIL: {asyncio.run(test_fail())}")

    laser.turn_laser(False)
    laser.turn_temperature(False)
