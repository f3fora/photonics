import asyncio
import logging
import time

import numpy as np
import serial
from serial.tools import list_ports

Instrument = serial.Serial
OSAParameters = {
    "port": list_ports.comports()[0].device,
    "baudrate": 9600,
    "timeout": 20,
    "bytesize": 8,
    "parity": "E",
    "stopbits": 1,
}


def s2b(some_string, end_line=b"\r\n", encode="ascii"):
    """string to binary"""
    return some_string.encode(encode) + end_line


def b2s(some_bytes, end_line=b"\r\n"):
    """binary to string"""
    return some_bytes.replace(end_line, b"").decode()


class OSAError(Exception):
    pass


class OSATimeOut(Exception):
    pass


class OSA(Instrument):
    def __init__(self, *args, **kwargs):
        self.index_file = None
        self.spectrum_file = None
        super().__init__(*args, **kwargs)

    def set_output_files(self, index, spectrum):
        self.index_file = index
        self.spectrum_file = spectrum

    def my_write(self, string: str):
        self.flushInput()
        self.flushOutput()
        return self.write(s2b(string))

    def my_read_until(self, expected=b"\r\n", size=None):
        self.flushInput()
        self.flushOutput()
        return self.read_until(expected=expected, size=size)

    def query(self, string: str, delay: float = 0.0, *args, **kwargs):
        self.my_write(string)
        time.sleep(delay)
        return self.my_read_until(*args, **kwargs)

    def errors(self, cmd="ERR?"):
        error = self.query(cmd)
        if int(error) > 0:
            self.flushInput()
            self.flushOutput()
            self.read(self.in_waiting)
            raise (OSAError(error))

        return True

    def clear(self, cmd="*CLR"):
        return self.my_write(cmd)

    def clear_measure(self, cmd="GCL"):
        return self.my_write(cmd)

    def buz_off(self, cmd="BUZ OFF"):
        return self.my_write(cmd)

    def set_wavelengths_range(self, min_wl, max_wl, cmd=("STA", "STO")):
        a = self.my_write(cmd[0] + " " + str(min_wl))
        a += self.my_write(cmd[1] + " " + str(max_wl))
        return a

    def get_wavelengths_range(self, cmd=("STA?", "STO?")):
        self.min_wl = float(self.query(cmd[0]))
        self.max_wl = float(self.query(cmd[1]))
        return self.min_wl, self.max_wl

    def set_sampling_points(self, n_points, cmd="MPT"):
        return self.my_write(cmd + " " + str(n_points))

    def get_sampling_points(self, cmd="MPT?"):
        self.n_points = int(self.query(cmd))
        return self.n_points

    def set_average_for_single_point(self, n_avg, cmd="AVT"):
        return self.my_write(cmd + " " + str(n_avg))

    def get_average_for_single_point(self, cmd="AVT?"):
        try:
            res = int(self.query(cmd))
        except:
            res = 1

        self.n_avg = res
        return self.n_avg

    def set_resolution(self, n, cmd="RES"):
        return self.my_write(cmd + " " + str(n))

    def get_resolution(self, cmd="RES?"):
        self.resolution = float(self.query(cmd))
        return self.resolution

    def mask_status_register(self, n, cmd="ESE2"):
        self.my_write(cmd + " " + n)
        return int(self.query(cmd + "?"))

    def get_status_register(self, cmd="ESR2?"):
        return int(self.query(cmd))

    def require_single_sweep(self, cmd="SSI"):
        return self.my_write(cmd)

    def get_data_from_memory(self, cmd="DBA?"):
        self.raw_data = self.query(cmd)
        return self.raw_data

    async def set_params(self, resolution, min_wl, max_wl, n_points, n_avg="OFF"):
        self.set_resolution(resolution)
        self.get_resolution()
        self.set_wavelengths_range(min_wl, max_wl)
        self.get_wavelengths_range()

        self.set_sampling_points(n_points)
        self.get_sampling_points()

        self.set_average_for_single_point(n_avg)
        self.get_average_for_single_point()

        return self.errors()

    async def get_spectrum(self):
        self.require_single_sweep()
        start_time = int(time.time_ns() * 1e-6)
        while True:
            if self.get_status_register() in [2, 3]:
                break

            await asyncio.sleep(0.5)

        self.get_data_from_memory()

        if self.in_waiting != 0:
            raise OSATimeOut(str(osa.in_waiting))

        # x = np.linspace(self.min_wl, self.max_wl, self.n_points)
        y = np.frombuffer(self.raw_data, np.dtype(np.int16).newbyteorder(">"))
        if self.spectrum_file is not None:
            np.savetxt(
                self.spectrum_file,
                y[: self.n_points].astype(int),
                fmt="%d",
                delimiter=",",
            )

        if self.index_file is not None:
            with open(self.index_file, "w+") as f:
                f.write(
                    f"{start_time},{self.min_wl},{self.max_wl},{self.n_points},{self.n_avg},{self.resolution}"
                )

        return (
            y[: self.n_points].astype(np.float64),
            self.min_wl,
            self.max_wl,
            self.n_points,
            self.n_avg,
        )


if __name__ == "__main__":
    osa = OSA(**OSAParameters)

    def test_wl():
        min_wl = 1300.0
        max_wl = 1500.0
        osa.set_wavelengths_range(min_wl, max_wl)
        return osa.get_wavelengths_range() == (min_wl, max_wl)

    def test_sp():
        sp = 501
        osa.set_sampling_points(sp)
        return osa.get_sampling_points() == sp

    def test_ap():
        ap = 4
        osa.set_average_for_single_point(ap)
        return osa.get_average_for_single_point() == ap

    def test_ese():
        n = 3
        return n == osa.mask_status_register(n)

    def test_ssi_and_dba():
        import time

        start_time = time.time()
        osa.require_single_sweep()
        while True:
            if osa.get_status_register() > 0:
                break

            time.sleep(1)

        logging.info(f"Time = {time.time() - start_time}")
        logging.info(f"Len of data = {len(osa.get_data_from_memory())}")
        return osa.in_waiting == 0

    async def test_params():
        logging.info("START SET PARAMETERS")
        task = asyncio.create_task(osa.set_params(0.1, 1300.0, 1500.0, 501, 4))
        logging.info("SETTING")
        x = await task
        logging.info("END SET")
        return x

    async def test_spectrum():
        logging.info("START SPECTRUM ACQUISITION")
        task = asyncio.create_task(osa.get_spectrum())
        logging.info("ACQUIRING")
        y, _, _, n_points, _ = await task
        logging.info("END ACQUISITION")
        return n_points == len(y)

    logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)
    logging.info("START")
    logging.info(f"Clear: {osa.clear()}")
    logging.info(f"Graphics Clear:  {osa.clear_measure()}")
    logging.info(f"Error: {osa.errors()}")
    logging.info(f"WaveLengths: {test_wl()}")
    logging.info(f"Sampling points: {test_sp()}")
    logging.info(f"Average point: {test_ap()}")
    logging.info(f"Status Register: {osa.get_status_register()}")
    logging.info(f"SSI: {test_ssi_and_dba()}")
    logging.info(f"Clear: {osa.clear()}")
    logging.info(f"Graphics Clear: {osa.clear_measure()}")
    logging.info("ASYNC FUNCTIONS:")
    logging.info(f"Test Parameter: {asyncio.run(test_params())}")
    logging.info(f"Test Spectum: {asyncio.run(test_spectrum())}")
    logging.info("END")
