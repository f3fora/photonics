import asyncio
import logging

from measure import ROOT, full_measure

if __name__ == "__main__":
    logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)
    logging.info(f"{ROOT}")
    asyncio.run(full_measure())
